<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Forget about your boss!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?=$metrika;?>
<style>
	* {
		margin: 0;
		padding: 0;
	}

	body {
		padding: 10px;
		text-align: center;
		min-height: calc(100vh - 2rem - 4rem);
		padding-bottom: 4rem;
	}

	p {
		border: solid 3px red;
		padding: 10px;
		width: 800px;
		max-width: 90%;
		display: block;
		margin: 2rem auto 4rem;
		color: #fff;
		text-align: center;
		font-size: 38px;
	}

	a {
		background: #12bd00;
		color: #fff;
		outline: none !important;
		border: none !important;
		text-decoration: none !important;
		cursor: pointer;
		padding: 15px 40px;
		font-size: 28px;
		text-align: center;
		border-radius: 10px;
		-webkit-box-shadow: 0 0 5px 0 rgba(0, 0, 0, .5);
		box-shadow: 0 0 5px 0 rgba(0, 0, 0, .5);
		display: block;
		width: 160px;
		margin: 0 auto;
	}

	a:hover {
		opacity: .5;
	}

	@media only screen and (max-width: 992px) {
		p {
			font-size: 26px;
		}

		body {
			padding-bottom: 0;
			padding: 0 10px;
		}
	}

</style>
<body style="background: url(asset/images/parallax2.jpg) no-repeat;">
<?=$pixel_img?>

	<p> We are glad that you decided to try our system out. It is better than traditional investing in many ways and it represents the Future paradigm of the global financial system. Your personal manager will call you shortly to share the details.</p>
	<a href="/">Home</a>
</body>

</html>
