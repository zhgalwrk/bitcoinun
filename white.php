<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Forget about your boss!</title>
	<style type="text/css">
		img.wp-smiley,
		img.emoji {
			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 .07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;
		}
	</style>

	<link rel='stylesheet' id='wp-block-library-css' href='/wp-includes/css/dist/block-library/style.min.css'
		type='text/css' media='all' />

	<style id='wp-block-library-inline-css' type='text/css'>
		.has-text-align-justify {
			text-align: justify;
		}
	</style>
	<link rel='stylesheet' id='uaf_client_css-css' href='/wp-content/uploads/useanyfont/uaf.css' type='text/css'
		media='all' />
	<link rel='stylesheet' id='parent-style-css'
		href='/wp-content/cache/autoptimize/css/autoptimize_single_4a2e663bd115ebb6a3c4dabf9ef508d0.css' type='text/css'
		media='all' />
	<link rel='stylesheet' id='divi-style-css'
		href='/wp-content/cache/autoptimize/css/autoptimize_single_f2b62d2c01f925e00d15fa9fa3b47892.css' type='text/css'
		media='all' />
	<link rel='stylesheet' id='et-builder-googlefonts-cached-css'
		href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%7CCabin%3Aregular%2Citalic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%7CAssistant%3A200%2C300%2Cregular%2C600%2C700%2C800%7CPT+Sans%3Aregular%2Citalic%2C700%2C700italic&#038;ver=5.4#038;subset=latin,latin-ext'
		type='text/css' media='all' />
	<link rel='stylesheet' id='addthis_all_pages-css'
		href='/wp-content/plugins/addthis/frontend/build/addthis_wordpress_public.min.css' type='text/css'
		media='all' />
	<link rel='stylesheet' id='dashicons-css' href='/wp-includes/css/dashicons.min.css' type='text/css' media='all' />
	<link rel='stylesheet' id='jetpack_css-css'
		href='/wp-content/cache/autoptimize/css/autoptimize_single_a84621c521bba913f3a756b031072d4c.css' type='text/css'
		media='all' />
	<script type='text/javascript'>
		/* <![CDATA[ */
		var monsterinsights_frontend = {
			"js_events_tracking": "true",
			"download_extensions": "doc,pdf,ppt,zip,xls,docx,pptx,xlsx",
			"inbound_paths": "[]",
			"home_url": "https:\/\/thebdschool.com",
			"hash_tracking": "false"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' async='async'
		src='/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js'></script>
	<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js'></script>
	<script type='text/javascript' async='async' src='/wp-includes/js/jquery/jquery-migrate.min.js'></script>
	<link rel='https://api.w.org/' href='/wp-json/' />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" />
	<meta name="generator" content="WordPress 5.4" />
	<link rel='shortlink' href='/?p=4575' />
	<link rel="alternate" type="application/json+oembed"
		href="/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fthebdschool.com%2Fbusiness-development-course%2F" />
	<link rel="alternate" type="text/xml+oembed"
		href="/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fthebdschool.com%2Fbusiness-development-course%2F&#038;format=xml" />

	<style type='text/css'>
		img#wpstats {
			display: none
		}
	</style>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<style type="text/css">
		/* If html does not have either class, do not show lazy loaded images. */
		html:not(.jetpack-lazy-images-js-enabled):not(.js) .jetpack-lazy-image {
			display: none;
		}
	</style>
	<script>
		document.documentElement.classList.add(
			'jetpack-lazy-images-js-enabled'
		);
	</script>
	<script>
		document.addEventListener('DOMContentLoaded', function (event) {

			if (window.location.hash) {
				// Start at top of page
				window.scrollTo(0, 0);

				// Prevent default scroll to anchor by hiding the target element
				var db_hash_elem = document.getElementById(window.location.hash.substring(1));
				window.db_location_hash_style = db_hash_elem.style.display;
				db_hash_elem.style.display = 'none';

				// After a short delay, display the element and scroll to it
				jQuery(function ($) {
					setTimeout(function () {
						$(window.location.hash).css('display', window.db_location_hash_style);
						et_pb_smooth_scroll($(window.location.hash), false, 800);
					}, 700);
				});
			}
		});
	</script>
	<link rel="icon"
		href="https://i2.wp.com/thebdschool.com/wp-content/uploads/2018/07/cropped-symbol-r-gradient-rbga.png?fit=32%2C32&#038;ssl=1"
		sizes="32x32" />
	<link rel="icon"
		href="https://i2.wp.com/thebdschool.com/wp-content/uploads/2018/07/cropped-symbol-r-gradient-rbga.png?fit=192%2C192&#038;ssl=1"
		sizes="192x192" />
	<link rel="apple-touch-icon"
		href="https://i2.wp.com/thebdschool.com/wp-content/uploads/2018/07/cropped-symbol-r-gradient-rbga.png?fit=180%2C180&#038;ssl=1" />
	<meta name="msapplication-TileImage"
		content="https://i2.wp.com/thebdschool.com/wp-content/uploads/2018/07/cropped-symbol-r-gradient-rbga.png?fit=270%2C270&#038;ssl=1" />
	<link rel="stylesheet" id="et-core-unified-cached-inline-styles"
		href="/wp-content/cache/et/4575/et-core-unified-15867662819926.min.css"
		onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" />
	<script id="mcjs">
		! function (c, h, i, m, p) {
			m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
		}(document, "script",
			"https://chimpstatic.com/mcjs-connected/js/users/928ddd3cbce8e04a0ba701801/6206145edb966fe75ef5e28bc.js");
	</script>
</head>


<body
	class="page-template page-template-page-template-blank page-template-page-template-blank-php page page-id-4575 et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_pb_pagebuilder_layout et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css">
	<div id="page-container">

		<div id="main-content">



			<article id="post-4575" class="post-4575 page type-page status-publish hentry">


				<div class="entry-content">
					<!-- <div class="at-above-post-page addthis_tool" data-url="/business-development-course/"></div> -->
					<div id="et-boc" class="et-boc">

						<div class="et_builder_inner_content et_pb_gutters3">
							<div
								class="et_pb_section et_pb_section_0 above-the-fold et_hover_enabled et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_0 et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div class="et_pb_module et_pb_image et_pb_image_0 et_always_center_on_mobile">


											<a href="#"><span class="et_pb_image_wrap "><img
														src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/07/logo-horizontal-gradient-white-rbga.png?w=1080&#038;ssl=1"
														alt data-recalc-dims="1"
														data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/07/logo-horizontal-gradient-white-rbga.png?w=1080&amp;is-pending-load=1#038;ssl=1"
														srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
														class=" jetpack-lazy-image"><noscript><img
															src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/07/logo-horizontal-gradient-white-rbga.png?w=1080&#038;ssl=1"
															alt="" data-recalc-dims="1" /></noscript></span></a>
										</div>
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_1 et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_1    et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h1>Start your business development career</h1>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_1 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span style="font-weight: 400;">Join our free 5-week online business
														development course and find your dream job!</span></p>
												<p><span style="font-weight: 400;">Next class starting June 6th,
														2020</span></p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_ et_pb_module ">
											<a class="et_pb_button et_pb_button_0 button-mobile-centre et_hover_enabled et_pb_bg_layout_dark"
												href="#course-check-out">Learn more</a>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_2    et_pb_css_mix_blend_mode_passthrough et_pb_column_empty">



									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_1 et_section_regular">




								<div class="et_pb_row et_pb_row_2">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_3    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>COVID-19 Update: to help you stay up to date we made all our courses
													for free until July 2020. The initiative is reserved for students
													living in the most affected areas. At the moment, it is available
													for students from Italy, China, and Iran. We are closely monitoring
													the situation and we don&#8217;t exclude taking extra measures as we
													go. Stay safe!</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_2 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_3 et_pb_equal_columns et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_2_5 et_pb_column_4    et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_3 center-align  et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>SOME FACTS</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_4 center-align  et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>Your future starts here</h2>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_5 center-align  et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>We are on a mission to create the next generation of business
													developers. We believe everyone is unique and we think business
													development is the right path for ambitious individuals who want to
													discover their true potential.</p>
												<p>We are here to teach you all we know to make you a successful
													business development professional!</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_3_5 et_pb_column_5    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_1 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-recruitment-illustration-small.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-recruitment-illustration-small.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-recruitment-illustration-small.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div
								class="et_pb_with_border et_pb_section et_pb_section_4 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_4 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_6    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_6 et_pb_bg_layout_light  et_pb_text_align_center">


											<div class="et_pb_text_inner">
												<p>OUR STUDENTS</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_7 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>That's the impact we aim for</h2>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_slider et_pb_slider_0 et_hover_enabled et_pb_slider_fullwidth_off et_pb_slider_no_pagination">
											<div class="et_pb_slides">
												<div class="et_pb_slide et_pb_slide_0 et_pb_bg_layout_light et_pb_media_alignment_center et-pb-active-slide"
													data-dots_color="#4D58DE" data-arrows_color="#4D58DE">


													<div class="et_pb_container clearfix">
														<div class="et_pb_slider_container_inner">

															<div class="et_pb_slide_description">
																<h2 class="et_pb_slide_title">“The course is
																	individually taken and tailored to your own needs,
																	both personal and business-wise. Taking the BD
																	course really helped me set a clear path for LUTTO’s
																	future.”</h2>
																<div class="et_pb_slide_content">
																	<p><img src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/09/sdfgjfh.png?resize=66%2C67&#038;ssl=1"
																			width="66" height="67"
																			alt="the bd school quirine wissink"
																			class="wp-image-6958 alignnone size-medium jetpack-lazy-image"
																			data-recalc-dims="1"
																			data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/09/sdfgjfh.png?resize=66%2C67&amp;is-pending-load=1#038;ssl=1"
																			srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"><noscript><img
																				src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/09/sdfgjfh.png?resize=66%2C67&#038;ssl=1"
																				width="66" height="67"
																				alt="the bd school quirine wissink"
																				class="wp-image-6958 alignnone size-medium"
																				data-recalc-dims="1" /></noscript></p>
																	<p><span color="#161720"
																			style="color: #161720;">Quirine
																			Wissink</span></p>
																	<p><span
																			style="color: #4d58de; font-size: small; letter-spacing: 1px;">FOUNDER
																			| LUTTO</span></p>
																	<p
																		style="color: #99c3eb; font-size: 16px; margin-top: -20px; letter-spacing: 1px; font-weight: 400;">
																</div>

															</div> <!-- .et_pb_slide_description -->
														</div>
													</div> <!-- .et_pb_container -->

												</div> <!-- .et_pb_slide -->
												<div class="et_pb_slide et_pb_slide_1 et_pb_bg_layout_light et_pb_media_alignment_center"
													data-dots_color="#4D58DE" data-arrows_color="#4D58DE">


													<div class="et_pb_container clearfix">
														<div class="et_pb_slider_container_inner">

															<div class="et_pb_slide_description">
																<h2 class="et_pb_slide_title">"The course was really
																	practical and gave me usable tools and format to
																	implement with clients and add value to my offer"
																</h2>
																<div class="et_pb_slide_content">
																	<p><img src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2020/01/59784141_10162034229730554_5746084647641546752_n.png?resize=66%2C69&#038;ssl=1"
																			width="66" height="69"
																			alt="the bd school testimonial erica pew"
																			class="wp-image-7193 alignnone size-medium jetpack-lazy-image"
																			data-recalc-dims="1"
																			data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2020/01/59784141_10162034229730554_5746084647641546752_n.png?resize=66%2C69&amp;is-pending-load=1#038;ssl=1"
																			srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"><noscript><img
																				src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2020/01/59784141_10162034229730554_5746084647641546752_n.png?resize=66%2C69&#038;ssl=1"
																				width="66" height="69"
																				alt="the bd school testimonial erica pew"
																				class="wp-image-7193 alignnone size-medium"
																				data-recalc-dims="1" /></noscript></p>
																	<p><span color="#161720"
																			style="color: #161720;">Erica Pew</span></p>
																	<p><span
																			style="color: #4d58de; font-size: small; letter-spacing: 1px;">BD
																			CONSULTANT | WELL HEELED CONSULTANCY</span>
																	</p>
																	<p
																		style="color: #99c3eb; font-size: 16px; margin-top: -20px; letter-spacing: 1px; font-weight: 400;">
																</div>

															</div> <!-- .et_pb_slide_description -->
														</div>
													</div> <!-- .et_pb_container -->

												</div> <!-- .et_pb_slide -->
												<div class="et_pb_slide et_pb_slide_2 et_pb_bg_layout_light et_pb_media_alignment_center"
													data-dots_color="#4D58DE" data-arrows_color="#4D58DE">


													<div class="et_pb_container clearfix">
														<div class="et_pb_slider_container_inner">

															<div class="et_pb_slide_description">
																<h2 class="et_pb_slide_title">“The BD School has truly
																	changed my way of working and helped me get all the
																	insights and strategies I needed to develop myself
																	and succeed in my career.”</h2>
																<div class="et_pb_slide_content">
																	<p><img src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-lluis-millet-escriva.png?resize=50%2C50&#038;ssl=1"
																			alt="thumb_01_90_90" width="50" height="50"
																			class="alignnone size-full wp-image-5984 jetpack-lazy-image"
																			data-recalc-dims="1"
																			data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-lluis-millet-escriva.png?resize=50%2C50&amp;is-pending-load=1#038;ssl=1"
																			srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"><noscript><img
																				src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-lluis-millet-escriva.png?resize=50%2C50&#038;ssl=1"
																				alt="thumb_01_90_90" width="50"
																				height="50"
																				class="alignnone size-full wp-image-5984"
																				data-recalc-dims="1" /></noscript></p>
																	<p><span style="color: #161720;">Lluis Millet
																			Escrivà</span></p>
																	<p
																		style="color: #99c3eb; font-size: 16px; margin-top: -20px; letter-spacing: 1px; font-weight: 400;">
																		<span
																			style="color: #4d58de; font-size: small;">BUSINESS
																			DEVELOPER | ALTISERVICE ENGIE</span></p>
																	<p
																		style="color: #99c3eb; font-size: 16px; margin-top: -20px; letter-spacing: 1px; font-weight: 400;">
																</div>

															</div> <!-- .et_pb_slide_description -->
														</div>
													</div> <!-- .et_pb_container -->

												</div> <!-- .et_pb_slide -->
												<div class="et_pb_slide et_pb_slide_3 et_pb_bg_layout_light et_pb_media_alignment_center"
													data-dots_color="#4D58DE" data-arrows_color="#4D58DE">


													<div class="et_pb_container clearfix">
														<div class="et_pb_slider_container_inner">

															<div class="et_pb_slide_description">
																<h2 class="et_pb_slide_title">“The BD School allowed us
																	to save a tremendous amount of time when training
																	our new hires. It also helped us improve our overall
																	strategy and achieve faster our goals.”</h2>
																<div class="et_pb_slide_content">
																	<p><img src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-juan-pablo-angarita.png?resize=55%2C55&#038;ssl=1"
																			alt="thumb_01_90_90" width="55" height="55"
																			class="alignnone size-full wp-image-5984 jetpack-lazy-image"
																			data-recalc-dims="1"
																			data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-juan-pablo-angarita.png?resize=55%2C55&amp;is-pending-load=1#038;ssl=1"
																			srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"><noscript><img
																				src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-juan-pablo-angarita.png?resize=55%2C55&#038;ssl=1"
																				alt="thumb_01_90_90" width="55"
																				height="55"
																				class="alignnone size-full wp-image-5984"
																				data-recalc-dims="1" /></noscript></p>
																	<p><span style="color: #161720;">Juan-Pablo
																			Angarita</span></p>
																	<p
																		style="color: #99c3eb; font-size: 16px; margin-top: -20px; letter-spacing: 1px; font-weight: 400;">
																		<span
																			style="color: #4d58de; font-size: small;">VP
																			SALES AND MARKETING | WIREMIND SAS</span>
																	</p>
																</div>

															</div> <!-- .et_pb_slide_description -->
														</div>
													</div> <!-- .et_pb_container -->

												</div> <!-- .et_pb_slide -->
												<div class="et_pb_slide et_pb_slide_4 et_pb_bg_layout_light et_pb_media_alignment_center"
													data-dots_color="#4D58DE" data-arrows_color="#4D58DE">


													<div class="et_pb_container clearfix">
														<div class="et_pb_slider_container_inner">

															<div class="et_pb_slide_description">
																<h2 class="et_pb_slide_title">“TBDS helped me shape a
																	better mindset about my business. They helped me
																	understand the importance of knowing my customers
																	and offering the right solution” </h2>
																<div class="et_pb_slide_content">
																	<p><img src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-rowan-de-geus.png?resize=59%2C59&#038;ssl=1"
																			alt="thumb_01_90_90" width="59" height="59"
																			class="alignnone size-full wp-image-5984 jetpack-lazy-image"
																			data-recalc-dims="1"
																			data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-rowan-de-geus.png?resize=59%2C59&amp;is-pending-load=1#038;ssl=1"
																			srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"><noscript><img
																				src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-rowan-de-geus.png?resize=59%2C59&#038;ssl=1"
																				alt="thumb_01_90_90" width="59"
																				height="59"
																				class="alignnone size-full wp-image-5984"
																				data-recalc-dims="1" /></noscript></p>
																	<p><span style="color: #161720;">Rowan de
																			Geus</span></p>
																	<p
																		style="color: #99c3eb; font-size: 16px; margin-top: -20px; letter-spacing: 1px; font-weight: 400;">
																		<span
																			style="color: #4d58de; font-size: small;">FOUNDER
																			| TRIKTRAK</span></p>
																</div>

															</div> <!-- .et_pb_slide_description -->
														</div>
													</div> <!-- .et_pb_container -->

												</div> <!-- .et_pb_slide -->

											</div> <!-- .et_pb_slides -->

										</div> <!-- .et_pb_slider -->

									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_5 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_5 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_7    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_8 et_pb_bg_layout_light  et_pb_text_align_center">


											<div class="et_pb_text_inner">
												<p>OUR PROMISE</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_9 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>Your success starts with education</h2>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_6 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_1_4 et_pb_column_8    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_2 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-peer-to-peer.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-peer-to-peer.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-peer-to-peer.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_10 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Practical</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_11 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>You will learn in a practical way
														all you need to start a career in business development</span>
												</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_4 et_pb_column_9    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_3 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-multidisciplinary.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-multidisciplinary.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-multidisciplinary.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_12 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Peer-2-peer</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_13 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Your teachers are business
														development professionals working in the field everyday</span>
												</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_4 et_pb_column_10    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_4 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-globe.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-globe.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-globe.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_14 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Online</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_15 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Follow the online course from
														wherever you are by scheduling a time that fits your
														schedule</span></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_4 et_pb_column_11    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_5 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-expertise.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-expertise.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-expertise.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_16 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Network</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_17 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Get access to our international
														network and find jobs in the hottest European startups </span>
												</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_6 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_7">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_12    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_18 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2 style="text-align: center;">Would you like more info? Contact us
													today!</h2>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_button_module_wrapper et_pb_button_1_wrapper et_pb_button_alignment_center et_pb_module ">
											<a class="et_pb_button et_pb_button_1 button-mobile-centre et_hover_enabled et_pb_bg_layout_dark"
												href="#course-check-out">Start learning now</a>
										</div>
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div
								class="et_pb_section et_pb_section_7 pa-hide-background-image-mobile et_section_regular">




								<div class="et_pb_row et_pb_row_8 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_13    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_19 et_pb_bg_layout_light  et_pb_text_align_center">


											<div class="et_pb_text_inner">
												<p>OVERVIEW</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_20 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>What you will learn</h2>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_21 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h3 style="text-align: center;">Next class starting February 29th 2020
												</h3>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_9 custom_row et_pb_equal_columns et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_14  second-on-mobile  et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_22 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>WEEK 1</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_23 center-align et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Fundamentals</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_24 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>In this first class, you will learn the basics of business
													development. Starting from its definition, all the way to career
													development. You will leave this session with a clear overview of
													what your future can potentially be.</p>
												<p>After this class you will:</p>
												<p><strong>Own the business development mindset</strong></p>
											</div>
										</div> <!-- .et_pb_text -->
										<div class="et_pb_module et_pb_image et_pb_image_6 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><noscript><img src alt></noscript><img
													class="lazyload" src alt><noscript><img src=""
														alt="" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_15  first-on-mobile  et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_7 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-1.jpg?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-1.jpg?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-1.jpg?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_10 et_pb_equal_columns et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_16    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_8 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-2.jpg?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-2.jpg?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-2.jpg?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_17    et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_25 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>WEEK 2</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_26 center-align et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Strategies</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_27 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span style="font-weight: 400;">In this class we will introduce you
														the main 4 business development strategies. You will learn the
														basics of sales, marketing, product and strategic partnerships.
														We will run practical exercises and use them on real business
														cases.</span></p>
												<p><span style="font-weight: 400;">After this class you will:</span></p>
												<p><strong>Own the basics of strategic business development</strong></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_11 custom_row et_pb_equal_columns et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_18  second-on-mobile  et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_28 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>WEEK 3</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_29 center-align et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Execution</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_30 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span style="font-weight: 400;">In this class, we will introduce you
														to the main business development tactics to identify your target
														audience, generate leads, approach potential clients and
														partners, build valuable connections and generate value. We will
														use roleplays and business cases.</span></p>
												<p><span style="font-weight: 400;">After this class you will:</span></p>
												<p><strong>Master the main business development tactics</strong></p>
											</div>
										</div> <!-- .et_pb_text -->
										<div class="et_pb_module et_pb_image et_pb_image_9 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><noscript><img src alt></noscript><img
													class="lazyload" src alt><noscript><img src=""
														alt="" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_19  first-on-mobile  et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_10 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-3.jpg?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-3.jpg?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-3.jpg?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_12 et_pb_equal_columns et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_20    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_11 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-4.jpg?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-4.jpg?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-4.jpg?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_21    et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_31 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>WEEK 4</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_32 center-align et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Soft skills</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_33 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span style="font-weight: 400;">In this class, we will introduce you
														to the main soft skills necessary for business development. We
														will practice research, communication, negotiation and how to be
														more productive. We will practice with roleplays, exercises and
														business cases.</span></p>
												<p><span style="font-weight: 400;">After this class you will:</span></p>
												<p><strong>Master the main business development skills</strong></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_13 custom_row et_pb_equal_columns et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_22  second-on-mobile  et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_34 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>WEEK 5</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_35 center-align et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Career </p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_36 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span style="font-weight: 400;">In this class, we will focus on
														career development. We will teach you how to find jobs in the
														digital era, how to restyle your CV, how to run a successful
														interview and how to build the career of your dreams.</span></p>
												<p><span style="font-weight: 400;">After this class you will:</span></p>
												<p><strong>Find your dream job</strong></p>
											</div>
										</div> <!-- .et_pb_text -->
										<div class="et_pb_module et_pb_image et_pb_image_12 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><noscript><img src alt></noscript><img
													class="lazyload" src alt><noscript><img src=""
														alt="" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_23  first-on-mobile  et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_13 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-5.jpg?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-5.jpg?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-5.jpg?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_8 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_14 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_24    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_37 et_pb_bg_layout_light  et_pb_text_align_center">


											<div class="et_pb_text_inner">
												<p>WHAT&#8217;S INCLUDED</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_38 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>Some tools to make you successful</h2>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_15 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_1_3 et_pb_column_25    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_14 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-chat.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-chat.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-chat.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_39 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Virtual classes</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_40 et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>You will meet your trainer online
														every week, for 2 hours for the entire length of the
														course</span></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_3 et_pb_column_26    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_15 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-qa.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-qa.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-qa.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_41 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Q&amp;A sessions</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_42 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Your trainer will be available for
														1 hour a week to answer any question or clarification</span></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_3 et_pb_column_27    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_16 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-homework.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-homework.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-homework.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_43 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Homework &amp; Exercises</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_44 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Every week, you will receive
														homework and exercise to practice what you just learned</span>
												</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_16 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_1_3 et_pb_column_28    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_17 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-certificate.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-certificate.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-certificate.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_45 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Certificate</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_46 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Upon completion of the Business
														development course, you will receive an official
														certification</span></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_3 et_pb_column_29    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_18 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-globe.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-globe.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-globe.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_47 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Access to our community</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_48 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>You will get access to our online
														community where you will meet other students</span></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_3 et_pb_column_30    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_19 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-lock-open.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-lock-open.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-lock-open.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_49 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Network</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_50 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>You will be connected with
														recruiters and career advisors to build your dream future</span>
												</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_9 et_section_regular">




								<div class="et_pb_row et_pb_row_17 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_31    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_51 et_pb_bg_layout_light  et_pb_text_align_center">


											<div class="et_pb_text_inner">
												<p>MEET YOUR TRAINER</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_52 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>Hi, I&#8217;m your coach!</h2>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_18 et_pb_equal_columns et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_2_5 et_pb_column_32    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_20 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/01/the-bd-school-lucia-min.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/01/the-bd-school-lucia-min.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/01/the-bd-school-lucia-min.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>

										<div class="et_pb_module et_pb_image et_pb_image_21 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><noscript><img src alt></noscript><img
													class="lazyload" src alt><noscript><img src=""
														alt="" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_3_5 et_pb_column_33    et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_53 center-align et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Lucia Piseddu</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_54 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span>I spent the last 10 years working in business development. In
														the coming weeks I&#8217;ll share my experience with you.
														Together we will go on an amazing journey that will give you
														solid basis to build the future you deserve!</span></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div id="course-check-out"
								class="et_pb_section et_pb_section_10 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_19">
									<div
										class="et_pb_column et_pb_column_2_5 et_pb_column_34    et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_55 center-align  et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>HOW MUCH DOES IT COST</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_56 center-align  et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>FREE</h2>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_57 center-align  et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span>5-week intensive course to build the career of your
														dreams!</span></p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_58 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<ul>
													<li>Pay only if hired*</li>
													<li>Learn a new job from scratch</li>
													<li>Homework &amp; Exercises</li>
													<li>Access to the community</li>
													<li>Access to recruitment network</li>
													<li>Certificate</li>
												</ul>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_3_5 et_pb_column_35    et_pb_css_mix_blend_mode_passthrough">



										<div id="et_pb_contact_form_0"
											class="et_pb_with_border et_pb_module et_pb_contact_form_0 et_hover_enabled et_pb_contact_form_container clearfix  et_pb_text_align_left"
											data-form_unique_num="0"
											data-redirect_url="https://calendly.com/lucia-tbds/call">


											<h1 class="et_pb_contact_main_title">Business Development Course</h1>
											<div class="et-pb-contact-message"></div>

											<div class="et_pb_contact">
												<form class="et_pb_contact_form clearfix xx_form" method="post"
													action="/thanks.php">
													<p class="et_pb_contact_field et_pb_contact_field_0 et_pb_contact_field_last"
														data-id="name" data-type="input">
														<label for="et_pb_contact_name_0"
															class="et_pb_contact_form_label">Name</label>
														<input type="text" id="et_pb_contact_name_0" class="input"
															value="" name="et_pb_contact_name_0"
															data-required_mark="required" data-field_type="input"
															data-original_id="name" placeholder="Name">
													</p>
													<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_last"
														data-id="company" data-type="input">
														<label for="et_pb_contact_company_0"
															class="et_pb_contact_form_label">Company name</label>
														<input type="text" id="et_pb_contact_company_0" class="input"
															value="" name="et_pb_contact_company_0"
															data-required_mark="required" data-field_type="input"
															data-original_id="company" placeholder="Company name">
													</p>
													<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_last"
														data-id="email" data-type="email">
														<label for="et_pb_contact_email_0"
															class="et_pb_contact_form_label">Email address</label>
														<input type="text" id="et_pb_contact_email_0" class="input"
															value="" name="et_pb_contact_email_0"
															data-required_mark="required" data-field_type="email"
															data-original_id="email" placeholder="Email address">
													</p>
													<p class="et_pb_contact_field et_pb_contact_field_3 et_pb_contact_field_last"
														data-id="phone" data-type="input">
														<label for="et_pb_contact_phone_0"
															class="et_pb_contact_form_label">Phone number</label>
														<input type="text" id="et_pb_contact_phone_0" class="input"
															value="" name="et_pb_contact_phone_0"
															data-required_mark="required" data-field_type="input"
															data-original_id="phone" placeholder="Phone number"
															pattern="[A-Z|a-z|0-9]*"
															title="Only letters and numbers allowed.">
													</p>
													<input type="text" value="" name="et_pb_contactform_validate_0"
														class="et_pb_contactform_validate_field" />
													<div class="et_contact_bottom_container">

														<button type="submit" class="et_pb_contact_submit et_pb_button"
															onclick="window.location = '/thanks.php'">APPLY</button>
													</div>
													
												</form>
											</div> <!-- .et_pb_contact -->
										</div> <!-- .et_pb_contact_form_container -->

									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_11 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_20 et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_36    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_59 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>Questions I had before taking the course</h2>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_21 et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_37    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_0 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">1. Who can enroll to this course?</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span color="#3a3c59">This course is designed for everyone who is
														ambitious and wants to go beyond their limits. Because the
														course is focused on career building, it is open to
														professionals looking for their next career step in business
														development. Our students are aware that there is always
														something more to achieve and they are not afraid to push their
														limits. You don&#8217;t need to be in business for a decade or
														even studied business related topics. All you need is to be
														hungry for success, be willing to invest on yourself and be open
														to the new opportunities that will come your way. </span></p>
												<p><span color="#3a3c59">Our mission is to help you find your true
														potential and teach you the skills and knowledge that will help
														you find the job of your dreams.</span><span color="#3a3c59">
														Business development is an amazing field. It&#8217;s creative,
														exciting and can really change your life in ways you don&#8217;t
														expect. If you are willin</span><span color="#3a3c59">g to
														learn, to challenge yourself and enter uncharted water then we
														are the right school for you.</span></p>
												<p><span color="#3a3c59">We will teach you all we know about business
														development with one goal: allowing you to build a brilliant
														career!</span></p>
											</div> <!-- .et_pb_toggle_content -->
										</div> <!-- .et_pb_toggle -->
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_1 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">2. How can I apply?</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>In order to be accepted in the course, you will have a first
														intake call to assess your case. </span></p>
												<p><span>If you pass the first screening you will need to send us your
														CV and a video explaining why you are fit for this
														program. </span></p>
												<p><span>We will then select the most inspiring stories from people who
														truly want to change their future. We will make these students
														the best business developers in the market and we will help them
														find the job they wish for. </span></p>
												<p>Do you have what it takes? Click apply or send an email to:<span> <a
															href="mailto:hello@thebdschool.com">hello@thebdschool.com</a> and
														we will be in touch soon!</span></p>
											</div>
										</div> 
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_2 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">3. When does the course start?</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>The next classes will start on <strong>January 11th,
															2020</strong> and it&#8217;s fully
														online.<strong> </strong>We are based in Rotterdam, The
														Netherlands. If you are based in a different time zone please
														contact us at <a
															href="mailto:hello@thebdschool.com">hello@thebdschool.com</a> and
														we will arrange the lectures to fit your needs. </span></p>
											</div>
										</div>
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_3 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">4. What is the modality of the course?</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>The course is online and can be done 100% remotely. The course
														lasts 5 weeks and every week you will meet your trainer for 2
														hours. In addition, every Thursday we host a 1-hour Q&amp;A
														session where you can ask clarifications or any other question
														about what you just learned. The session is hosted weekly in our
														exclusive student&#8217;s community. </span></p>
											</div> 
										</div>
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_4 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">5. How many hours per week does the course
												require</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>In order to successfully complete the course, you will have to
														follow each lesson every week over the course of 4 weeks. The
														duration of each lesson is 2 hours. Each class includes homework
														that you will have to execute by yourself in the days following
														the class. The estimated time to complete the homework is around
														2 hours. In total, you will invest about 20 hours over the
														course of 5 weeks. At the end of the course you will take part
														to a final test. If you successfully pass the test you will
														receive a certificate of completion that you can use to increase
														your value in the market.</span></p>
											</div>
										</div> 
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_5 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">6. What happens after I enroll to the course
											</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>After successfully enrolling to the course you will receive an
														email by one of our trainers. This email will include a link to
														your first lecture and some course materials to prepare. Our
														trainers will keep in touch throughout the entire length of the
														course, making sure everything is easy for you. You can contact
														us at any time using the &#8220;Contact us&#8221; form or by
														sending an email to <a
															href="mailto:hello@thebdschool.com">hello@thebdschool.com</a>. </span>
												</p>
											</div>
										</div>
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_6 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">7. How much does the course cost?</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>After receiving the applications, we will carefully review them
														and select the stories that inspired us the most. Because our
														mission is to enable individuals to build successful business
														development careers, the course is <strong>completely
															free</strong>. You will only pay when you will find a job in
														the following months. The cost is <strong>1.499,00</strong> €
														that you will pay after starting working at your new employer. 
														If you don&#8217;t find a job, the course is on us. We believe
														in you and we want to be part of your success sharing all we
														know about business development.</span></p>
											</div>
										</div> 
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_7 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">8. Can I opt for installment payments?</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>Yes, we have an installment plan! After successfully finding a
														job, you can pay the course in 3 months (€500 per month). You
														will sign a payment agreement when you will enroll in the
														course. </span></p>
											</div> 
										</div>
									</div> 


								</div>


							</div>
							<div class="et_pb_section et_pb_section_12 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_22">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_38    et_pb_css_mix_blend_mode_passthrough et-last-child">
										<div
											class="et_pb_module et_pb_text et_pb_text_60 et_pb_bg_layout_light  et_pb_text_align_left">
											<div class="et_pb_text_inner">
												<h2 style="text-align: center;">Ready to start your learning journey?
												</h2>
											</div>
										</div>
										<div
											class="et_pb_button_module_wrapper et_pb_button_2_wrapper et_pb_button_alignment_center et_pb_module ">
											<a class="et_pb_button et_pb_button_2 button-mobile-centre et_hover_enabled et_pb_bg_layout_dark"
												href="#course-check-out">Enroll</a>
										</div>
									</div>
								</div>
							</div>
							<div class="et_pb_section et_pb_section_16 et_pb_with_background et_section_regular">

<style>
	.footer-policy{
		font-weight: bold;
		font-size: 22px;
		transition: .3s;
		text-decoration: underline;
	}
	.footer-policy:hover{
		color: #333;
		text-decoration: underline;

	}
	.footer-phone{
		display: flex;
		flex-direction: column-reverse;
		padding-top: 10px;

	}
</style>
								<div class="et_pb_row et_pb_row_24">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_44 et_pb_css_mix_blend_mode_passthrough">
										<a href="./Privacy.html" class="footer-policy">Privacy policy</a>
										
									</div>
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_44 et_pb_css_mix_blend_mode_passthrough">
										<p>Suite 811 Tsimshatsui Centre, East Wing, 66 Mody Road, Tsimshatsui East, Kowloon, Hong Kong</p>
										<div class="footer-phone">
											<span>+852 8595 9955</span>
											<span>+852 7589 4343</span>
										</div>
									</div>
									
								</div>
								<br>
								<br>
								<div class="et_pb_row et_pb_row_24">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_43 et_pb_css_mix_blend_mode_passthrough">
										<div
											class="et_pb_module et_pb_text et_pb_text_67 center-align  et_pb_bg_layout_light  et_pb_text_align_right">
											<div class="et_pb_text_inner">
												<p style="text-align: left;">©2020 The BD School. All rights reserved
												</p>
											</div>
										</div>
									</div>
								

								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
		</div>
	</div>


	<style>
		.lazyload,
		.lazyloading {
			opacity: 0;
		}

		.lazyloaded {
			opacity: 1;
			transition: opacity 300ms;
		}
	</style>

	<noscript>
		<style>
			.lazyload {
				display: none;
			}
		</style>
	</noscript>

	<script data-noptimize="1">
		window.lazySizesConfig = window.lazySizesConfig || {};
		window.lazySizesConfig.loadMode = 1;
	</script>
	<script async data-noptimize="1"
		src='/wp-content/plugins/autoptimize/classes/external/js/lazysizes.min.js?ao_version=2.6.2'></script>
	<script data-noptimize="1">
		function c_webp(A) {
			var n = new Image;
			n.onload = function () {
				var e = 0 < n.width && 0 < n.height;
				A(e)
			}, n.onerror = function () {
				A(!1)
			}, n.src = 'data:image/webp;base64,UklGRhoAAABXRUJQVlA4TA0AAAAvAAAAEAcQERGIiP4HAA=='
		}

		function s_webp(e) {
			window.supportsWebP = e
		}
		c_webp(s_webp);
		document.addEventListener('lazybeforeunveil', function ({
			target: c
		}) {
			supportsWebP && ['data-src', 'data-srcset'].forEach(function (a) {
				attr = c.getAttribute(a), null !== attr && c.setAttribute(a, attr.replace(/\/client\//,
					'/client/to_webp,'))
			})
		});
	</script>
	<script>
		(function () {
			// Override the addClass to prevent fixed header class from being added
			var addclass = jQuery.fn.addClass;
			jQuery.fn.addClass = function () {
				var result = addclass.apply(this, arguments);
				jQuery('#main-header').removeClass('et-fixed-header');
				return result;
			}
		})();
		jQuery(function ($) {
			$('#main-header').removeClass('et-fixed-header');
		});
	</script>
	<script data-cfasync="false" type="text/javascript">
		if (window.addthis_product === undefined) {
			window.addthis_product = "wpp";
		}
		if (window.wp_product_version === undefined) {
			window.wp_product_version = "wpp-6.2.6";
		}
		if (window.addthis_share === undefined) {
			window.addthis_share = {};
		}
		if (window.addthis_config === undefined) {
			window.addthis_config = {
				"data_track_clickback": true,
				"ui_atversion": "300"
			};
		}
		if (window.addthis_plugin_info === undefined) {
			window.addthis_plugin_info = {
				"info_status": "enabled",
				"cms_name": "WordPress",
				"plugin_name": "Share Buttons by AddThis",
				"plugin_version": "6.2.6",
				"plugin_mode": "AddThis",
				"anonymous_profile_id": "wp-caaf6d80e67ecb82cbb72516e6643c75",
				"page_info": {
					"template": "pages",
					"post_type": ""
				},
				"sharing_enabled_on_post_via_metabox": false
			};
		}
		(function () {
			var first_load_interval_id = setInterval(function () {
				if (typeof window.addthis !== 'undefined') {
					window.clearInterval(first_load_interval_id);
					if (typeof window.addthis_layers !== 'undefined' && Object.getOwnPropertyNames(window
							.addthis_layers).length > 0) {
						window.addthis.layers(window.addthis_layers);
					}
					if (Array.isArray(window.addthis_layers_tools)) {
						for (i = 0; i < window.addthis_layers_tools.length; i++) {
							window.addthis.layers(window.addthis_layers_tools[i]);
						}
					}
				}
			}, 1000)
		}());
	</script>
	<script type='text/javascript' async='async'
		src='/wp-content/plugins/jetpack/_inc/build/photon/photon.min.js?ver=20191001'></script>
	<script type='text/javascript'>
		var leadin_wordpress = {
			"userRole": "visitor",
			"pageType": "page",
			"leadinPluginVersion": "7.25.2"
		};
	</script>
	
	<script type='text/javascript'>
		var DIVI = {
			"item_count": "%d Item",
			"items_count": "%d Items"
		};
		var et_shortcodes_strings = {
			"previous": "Previous",
			"next": "Next"
		};
		var et_pb_custom = {
			"images_uri": "https:\/\/thebdschool.com\/wp-content\/themes\/Divi\/images",
			"builder_images_uri": "https:\/\/thebdschool.com\/wp-content\/themes\/Divi\/includes\/builder\/images",
			"et_frontend_nonce": "fe4ca74448",
			"subscription_failed": "Please, check the fields below to make sure you entered the correct information.",
			"et_ab_log_nonce": "ac70f978db",
			"fill_message": "Please, fill in the following fields:",
			"contact_error_message": "Please, fix the following errors:",
			"invalid": "Invalid email",
			"captcha": "Captcha",
			"prev": "Prev",
			"previous": "Previous",
			"next": "Next",
			"wrong_captcha": "You entered the wrong number in captcha.",
			"ignore_waypoints": "no",
			"is_divi_theme_used": "1",
			"widget_search_selector": ".widget_search",
			"is_ab_testing_active": "",
			"page_id": "4575",
			"unique_test_id": "",
			"ab_bounce_rate": "5",
			"is_cache_plugin_active": "no",
			"is_shortcode_tracking": "",
			"tinymce_uri": ""
		};
		var et_pb_box_shadow_elements = [];
	</script>
	<script type='text/javascript' async='async' src='/wp-content/themes/Divi/js/custom.min.js'></script>
	
	<script type='text/javascript' async='async'
		src='/wp-content/plugins/jetpack/_inc/build/lazy-images/js/lazy-images.min.js'></script>
	<script type='text/javascript' async='async'
		src='/wp-content/cache/autoptimize/js/autoptimize_single_82b34a0f20682b94458a89521a92c7ca.js'>
	</script>
	<script type='text/javascript' async='async' src='/wp-includes/js/wp-embed.min.js'></script>
	<script type='text/javascript' src='https://stats.wp.com/e-202016.js' async='async' defer='defer'></script>
	<script type='text/javascript'>
		_stq = window._stq || [];
		_stq.push(['view', {
			v: 'ext',
			j: '1:8.3',
			blog: '149801255',
			post: '4575',
			tz: '0',
			srv: 'thebdschool.com'
		}]);
		_stq.push(['clickTrackerInit', '149801255', '4575']);
	</script>
</body>

</html>

